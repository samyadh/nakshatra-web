import { PaymentService } from './services/payment.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import 'jarallax';
declare var jarallax: any;
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	host: {
		'(window:load)': 'windowLoaded()'
	}
})
export class AppComponent implements OnInit {

	param = { value: 'world' };
	loading = true;
	paymentIsProcessing = false;

	constructor(translateService: TranslateService, private paymentService: PaymentService) {
		translateService.setDefaultLang('en');
		translateService.use('en');
	}

	ngOnInit() {
		this.paymentService.paymentProcessing.subscribe((isPaymentProcessing) => {
			this.paymentIsProcessing = isPaymentProcessing;
		});
	}

	windowLoaded() {
		setTimeout(() => {
			this.loading = false;
		}, 1500);
	}


}
