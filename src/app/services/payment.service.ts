import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Injectable({
	providedIn: 'root'
})
export class PaymentService {
	paymentProcessing: BehaviorSubject<boolean> = new BehaviorSubject(false);

	constructor(private httpClient: HttpClient) {
		this.paymentProcessing.next(false);
	}

	togglePayment() {
		if (!this.paymentProcessing.getValue()) {
			this.paymentProcessing.next(true);
		} else {
			this.paymentProcessing.next(false);
		}
	}

	getOrderID() {
		return this.httpClient.get(environment.baseUrl + 'getOrder.php').toPromise();
	}

	sendEmail(value: any) {
		return this.httpClient.post(environment.baseUrl + 'sendEmail.php', value).toPromise();
	}
}
