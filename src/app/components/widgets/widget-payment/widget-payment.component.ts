import { PaymentService } from './../../../services/payment.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { WindowRef } from 'src/app/services/window-ref';
import { FormControl, FormBuilder, Validators, FormGroup, Form } from '@angular/forms';
@Component({
	selector: 'app-widget-payment',
	templateUrl: './widget-payment.component.html',
	styleUrls: ['./widget-payment.component.scss']
})
export class WidgetPaymentComponent implements OnInit {
	activeStep = 1;
	orderDetails: FormGroup;
	formValue: any = '';
	location = '';
	dateToShow = '';
	peopleInvolved: any = [];
	delivieryAdress = true;
	isPreview = false;
	options: any = {
		'key': 'rzp_test_7Y3wN2zsFtULNE',
		'amount': '300000',
		'currency': 'INR',
		'name': 'The Nakshatra',
		'description': 'The Nakshatra | treasuring your moments.',
		'image': 'https://www.thenakshatra.com/assets/images/fevicon.png',
		'order_id': '',
		'handler': function (response, error) {
			if (error) {
				document.getElementById('errorPayment').click();
				this.activeStep = 5;
			}
			let successEle = (document.getElementById('successPayment') as HTMLInputElement);
			successEle.value = response.razorpay_payment_id;
			successEle.click();
		},
		'prefill': {
			'name': '',
			'email': '',
			'contact': ''
		},
		'theme': {
			'color': '#F37254'
		}
	};
	rzp1: any;
	myComponent = this;

	@ViewChild('datetime', { static: false }) datetimeRef: ElementRef;

	constructor(private paymentService: PaymentService, private winRef: WindowRef, private _fb: FormBuilder) {
		this.orderDetails = this._fb.group({
			deliveryAddress: this._fb.group({
				fullname: ['', Validators.required],
				email: ['', [Validators.required, Validators.email]],
				address: ['', Validators.required],
				city: ['', Validators.required],
				zipcode: ['', Validators.required],
				phone: ['', Validators.required]
			}),
			billingAddress: this._fb.group({
				fullname: ['', Validators.required],
				email: ['', [Validators.required, Validators.email]],
				address: ['', Validators.required],
				city: ['', Validators.required],
				zipcode: ['', Validators.required],
				phone: ['', Validators.required]
			}),
			submissionDetails: this._fb.group({
				peopleInvolved: ['', [Validators.required, Validators.pattern(/^(([\w\s]{1,12}){1}(\s&\s){1}([\w\s]{1,12}){1}){1}$|^([\w\s]{1,12}\s?&?\s?){1}$/i)]],
				customText: ['', Validators.required],
				dateTime: ['', Validators.required],
				location: ['', Validators.required],
				frameColor: ['wood', Validators.required],
				backgroundColor: ['black', Validators.required],
				paymentId: ['']
			}),
		});
	}

	ngOnInit() {

	}

	myResponseHandler(event) {
		if (event.target.id === 'errorPayment') {
			this.activeStep = 5;
			return false;
		} else {
			this.orderDetails.get('submissionDetails').get('paymentId').setValue(event.target.value);
			this.orderDetails.get('submissionDetails').get('dateTime').setValue(this.dateToShow);
			this.paymentService.sendEmail(this.orderDetails.value).then((emailDetails: any) => {
				if (emailDetails.success) {
					this.activeStep = 4;
					this.resetForm();
				} else {
					this.activeStep = 5;
				}
			});
		}
	}

	changePeopleInvolved(event) {
		let nameValue = event.target.value;
		if (nameValue.length > 0 && event.keyCode !== 8) {
			if (nameValue.indexOf('&') > 0) {
				if (nameValue.length >= 28) {
					return false;
				}
			} else {
				if (nameValue.length >= 14) {
					return false;
				}
			}
		}
	}

	updatePeopleInvolved(event) {
		this.peopleInvolved = event.target.value.trim().split('&');
	}

	cancelPayment() {
		this.paymentService.togglePayment();
		this.activeStep = 1;
		this.peopleInvolved = [];
		this.location = '';
		this.dateToShow = '';
		this.orderDetails.reset();
		this.orderDetails.get('submissionDetails').patchValue({ 'frameColor': 'wood', 'backgroundColor': 'black' });
	}

	resetForm() {
		let { frameColor, backgroundColor, customText } = this.orderDetails.get('submissionDetails').value;
		this.orderDetails.reset();
		this.orderDetails.get('submissionDetails').patchValue({ 'customText': customText, 'frameColor': frameColor, 'backgroundColor': backgroundColor });
	}

	formSubmited(formType) {

		if (formType === 1) {
			if (!this.orderDetails.get('submissionDetails').valid) {
				return false;
			}
			this.activeStep++;
		}

		if (formType === 2) {
			if (!this.orderDetails.get('deliveryAddress').valid && !this.orderDetails.get('billingAddress').valid) {
				return false;
			}
			this.activeStep++;
		}

		if (formType === 3) {
			this.paymentService.getOrderID().then((orderDetails: any) => {
				this.options.order_id = orderDetails.orderId;
				this.options.prefill = {
					name: this.orderDetails.value.billingAddress.fullname,
					email: this.orderDetails.value.billingAddress.email,
					contact: this.orderDetails.value.billingAddress.phone
				};
				this.rzp1 = new this.winRef.nativeWindow.Razorpay(this.options);
				this.rzp1.open();
			});
		}

		if (formType === 5) {
			this.rzp1 = new this.winRef.nativeWindow.Razorpay(this.options);
			this.rzp1.open();
			this.activeStep = 1;
		}
	}

	public handleAddressChange(address: Address) {
		this.location = address.formatted_address.split(',')[0];
		this.orderDetails.get('submissionDetails').get('location').patchValue(address.formatted_address);

	}

	copySameToBilling() {
		this.orderDetails.get('billingAddress').patchValue(this.orderDetails.get('deliveryAddress').value);
	}

	formatTime(event) {
		var myDate = new Date(this.orderDetails.get('submissionDetails').get('dateTime').value);
		this.dateToShow = myDate.getDate() + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear() + ' at ' + myDate.getHours() + ':' + myDate.getMinutes() + ' Hrs';
	}

	addAnotherPerson(event) {
		this.orderDetails.controls['submissionDetails'].get('peopleInvolved').setValue(this.peopleInvolved[0].trim() + ' & ');
		event.target.previousElementSibling.focus();
	}

}
