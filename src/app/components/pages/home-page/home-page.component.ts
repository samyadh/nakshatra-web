import { PaymentService } from './../../../services/payment.service';
import { Component, OnInit } from '@angular/core';


@Component({
	selector: 'app-home-page',
	templateUrl: './home-page.component.html',
	styleUrls: ['./home-page.component.scss'],

})
export class HomePageComponent implements OnInit {

	config: any;
	fullpageApi: any;

	constructor(public paymentService: PaymentService) { }

	ngOnInit() {
		this.config = {
			licenseKey: '123123123',
			responsiveWidth: 991
		};
		this.paymentService.paymentProcessing.subscribe((isProccessing) => {
			if (this.fullpageApi) {
				if (isProccessing) {
					this.fullpageApi.setAllowScrolling(false);
				} else {
					this.fullpageApi.setAllowScrolling(true);
				}
			}
		});
	}

	getRef(fullPageRef) {
		this.fullpageApi = fullPageRef;
		console.log(fullPageRef);
	}

}
