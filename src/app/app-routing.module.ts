import { ContactPageComponent } from './components/pages/contact-page/contact-page.component';
import { ExpertPageComponent } from './components/pages/expert-page/expert-page.component';
import { PolicyPageComponent } from './components/pages/policy-page/policy-page.component';
import { TermsPageComponent } from './components/pages/terms-page/terms-page.component';
import { HomePageComponent } from './components/pages/home-page/home-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [

	{
		path: 'home',
		component: HomePageComponent
	},
	{
		path: 'contact',
		component: ContactPageComponent
	},
	{
		path: 'expert',
		component: ExpertPageComponent
	},
	{
		path: 'privacy',
		component: PolicyPageComponent
	},
	{
		path: 'terms',
		component: TermsPageComponent
	},
	{
		path: '',
		redirectTo: 'home',
		pathMatch: 'full'
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {
		scrollPositionRestoration: 'top'
	})],
	exports: [RouterModule]
})
export class AppRoutingModule { }
