import { WindowRef } from './services/window-ref';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslateModule, TranslateLoader, TranslatePipe } from '@ngx-translate/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './components/pages/home-page/home-page.component';
import { PolicyPageComponent } from './components/pages/policy-page/policy-page.component';
import { ContactPageComponent } from './components/pages/contact-page/contact-page.component';
import { ExpertPageComponent } from './components/pages/expert-page/expert-page.component';
import { TermsPageComponent } from './components/pages/terms-page/terms-page.component';
import { WebpackTranslateLoader } from './webpack-translate-loader.loader';
import { HeaderComponent } from './components/common/header/header.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { AngularFullpageModule } from '@fullpage/angular-fullpage';
import { WidgetPaymentComponent } from './components/widgets/widget-payment/widget-payment.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
	declarations: [
		AppComponent,
		HomePageComponent,
		PolicyPageComponent,
		ContactPageComponent,
		ExpertPageComponent,
		TermsPageComponent,
		HeaderComponent,
		FooterComponent,
		WidgetPaymentComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		AngularFullpageModule,
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
		FontAwesomeModule,
		FormsModule,
		ReactiveFormsModule,
		GooglePlaceModule,
		HttpClientModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useClass: WebpackTranslateLoader
			},
			isolate: false
		}),
		BrowserAnimationsModule
	],
	exports: [

	],
	providers: [WindowRef],
	bootstrap: [AppComponent]
})
export class AppModule { }
